//Mandeep Singh 1937332
package movies.importer;

import java.util.ArrayList;

public class RemoveDuplicates extends Processor {
	
	public RemoveDuplicates (String sourceDir, String outDir) {
		super(sourceDir,outDir,false);
	}
	
	public ArrayList <String> process(ArrayList <String> input) {
		
		ArrayList <String> withoutDuplicate_input = new ArrayList <String>();
		
		for(String text : input) {
			
			if(!withoutDuplicate_input.contains(text)) {
				
				withoutDuplicate_input.add(text);
				
			}
			
			
		}
		
		return withoutDuplicate_input;
		
	}

}
