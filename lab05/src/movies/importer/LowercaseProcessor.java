//Mandeep Singh 1937332
package movies.importer;
import java.util.*;

public class LowercaseProcessor extends Processor{
	
	public LowercaseProcessor(String sourceDirectory,String destinationDirectory) {
		
		super(sourceDirectory,destinationDirectory,true);
	}
	
	public ArrayList<String> process(ArrayList<String> input){
		
		ArrayList<String> asLower = new ArrayList <String>();
		
		for(int i=0; i<input.size();i++) {
			asLower.add(input.get(i).toLowerCase());
			
		}
		
		return asLower;
		
	}

}
